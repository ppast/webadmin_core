<?php

// namespace
namespace Ppast\Webadmin\WebInterface;



// clauses use
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Includes\IncludesHelper;
use \Ppast\Webadmin\Auth\AuthHandler;



// encodage
header("Content-Type: text/html; charset=utf-8");
	
// afficher les erreurs
ini_set('display_errors', 'stdout');

// initialiser fuseau horaire (obligatoire depuis php 5.5.10)
ini_set('date.timezone', 'Europe/Paris');

// forcer encodage
mb_internal_encoding("utf-8");

// définir locale
setlocale(LC_TIME, 'fr_FR.utf8');




// controller interface principal
class CoreController 
{
	// utilisateur identifié ?
	static $auth = NULL;
	
	// routage vers script utilisateur appelant
	static $routage = NULL;
	
	// user et contexte requetes
	static $USER = NULL;
	
	// commande en cours
	static $command = NULL;



	// appel commande
	static function coreCommandHandler($nscmd, $cmd, $params)
	{
		// exécution de la commande et récup résultat normalisé dans ST
		if ( $cmd )
		{
			$cmdclass = $nscmd . ucfirst($cmd);
			self::$command = new $cmdclass();
			return call_user_func_array(array(self::$command, 'run'), $params);
		}
		else
			return NULL;
	}


	// traitement de base : sanitize, init config CORE et AuthHandler, USER, routage, rootConfig et init users
	static function coreProcess($data_root, $controller_file)
	{
		// nettoyer requete
		IncludesHelper::cm_sanitize_array($_REQUEST);
		

		// initialiser des globales et classes statiques
		// -- racine www
		Core::$ROOT = IncludesHelper::ensureTrailingSlash($_SERVER['DOCUMENT_ROOT']);
		// -- chemin vers webadmin (console ou pas, déduit en fonction du controller)
		Core::$WEBADMIN_ROOT = IncludesHelper::ensureTrailingSlash(realpath(IncludesHelper::ensureTrailingSlash(dirname($controller_file)) . '../'));
        // -- chemin vers webadmin_core
        Core::$WEBADMIN_CORE_ROOT = IncludesHelper::ensureTrailingSlash(realpath(IncludesHelper::ensureTrailingSlash(__DIR__) . '../'));
		// -- chemin vers config utilisateur
		Core::$DATA_ROOT = IncludesHelper::ensureTrailingSlash($data_root);
		// -- chemin vers dossier appelant
		Core::$CALLER_ROOT = IncludesHelper::ensureTrailingSlash(dirname($_SERVER['SCRIPT_FILENAME']));

		// -- nom cookie csrf, forgé depuis nom du controller
		AuthHandler::$CSRFCookieName = md5($controller_file) . '__CSRF__';



		// init user et contexte par défaut
		self::$USER = isset($_REQUEST['u']) ? $_REQUEST['u'] : '';
		self::$routage = $_SERVER['SCRIPT_NAME'];


		// config root (webadmin et user-defined)
		Core::rootConfig();
		
		
		// fichier des utilisateurs
		\Ppast\Webadmin\Config\Users::setupUsersProvider();
	}
}

?>