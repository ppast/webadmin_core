<?php

// namespace
namespace Ppast\Webadmin\Auth;


// clauses use
use \Ppast\Webadmin\Includes\SecureRequestHelper;
use \Ppast\Webadmin\Includes\RequestSecurityHelper;
use \Ppast\Webadmin\Config\Users;
use \Ppast\Webadmin\Includes\CSRFException;



// classe
class AuthHandler
{
	// helper csrf
	private static $_csrfhelper = NULL;
	
	// nom cookie
	static $CSRFCookieName = NULL;
	
	
	
	// obtenir helper
	static function getCSRFHelper()
	{
		if ( !self::$_csrfhelper )
			self::$_csrfhelper = new SecureRequestHelper(self::$CSRFCookieName, 'token', basename(__FILE__));
			
		return self::$_csrfhelper;
	}
	
	
	// arrêter
	static function halt()
	{
		self::getCSRFHelper()->revokeCSRF();
		return FALSE;
	}
	
	
	// authentifier l'utilisateur pendant une requête
	static function authenticateRequest($elements)
	{
		// vérifier concordance entre token=hmac_sha256(cookie)
		try
		{
		    self::getCSRFHelper()->authorizeCSRF($elements);
		    return true;
		}
		catch(CSRFException $e)
		{
		    return self::halt();
		}
	}
	
	
	// authentifier l'utilisateur arrivé par login URL avec un user, un password crypté spécifiquement et un jeton horaire
	static function authenticateUrl($u, $urlpasswd, $urlts)
	{
		// lire timestamp (2nde partie urlts)
		$ts = substr($urlts, 64);
		
		// si jeton timestamp OK
		if ( ($urlts == hash_hmac('sha256', 'ts!' . hexdec($ts), 'webAdminAuth|authenticateUrl') . $ts)
			 &&
			 (time() < hexdec($ts) )		// vérifier que l'heure courante ne dépasse pas le timestamp (qui contient déjà +30 secondes)
			)
			return self::authenticate($u, $urlpasswd);
		else
			return self::halt();
	}
	
	
	// authentifier l'utilisateur
	static function authenticate($ulogin, $upasswd)
	{
		// si authentification OK
		if ( Users::usersProvider()->testUser($ulogin, $upasswd) )
		{
			self::getCSRFHelper()->initializeCSRF();
			return true;
		}
		else
			return self::halt();
	}
	
	
	// authentifier l'utilisateur par formulaire
	static function authenticateForm($ulogin, $upasswd, $formtoken)
	{
		if ( RequestSecurityHelper::checkFormToken($formtoken) )
			return self::authenticate($ulogin, $upasswd);
			
		return self::halt();
	}
}

?>