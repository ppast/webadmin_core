<?php

// namespace
namespace Ppast\Webadmin\Auth;


// clauses use



// classe
abstract class UsersProvider
{
	// énumérer les utilisateurs
	abstract function listUsers();
	
	
	// ajouter un utilisateur
	abstract function createUser(\stdClass $u);
    
    
    // obtenir les propriétés liées à l'utilisateur
	abstract function userProperties();
	
	
	// effacer un utilisateur
	abstract function removeUser($uname);
	
	
	// modifier un utilisateur
	abstract function updateUser($uname, \stdClass $u);
	
	
	// tester un utilisateur
	public function testUser($uname, $passwd)
	{
		// lister les utilisateurs
		$users = $this->listUsers();
		
		// si user pas connu
		if ( !array_key_exists($uname, $users) )
			return FALSE;
		
		return $users[$uname]->password === $passwd;
	}
	
	
	// utilisateur autorisé ; préciser un ou plusieurs niveaux d'accès possible, au choix
	public function isUserAuthorized($uname, $accesslevels)
	{
		// lister les utilisateurs
		$users = $this->listUsers();
		
		// si user pas connu
		if ( !array_key_exists($uname, $users) )
			return FALSE;
		
		// extraire roles du user 
		$roles = $users[$uname]->roles;
		if ( !$roles )
			$roles = array();
        else    
            $roles = explode(',', $roles);

        
		// pour tous les rôles au choix proposés, regarder si l'un est associé au user
		foreach ( $accesslevels as $alevel )
			if ( in_array($alevel, $roles) )
				return TRUE;
				
		return FALSE;
	}
	
}

?>