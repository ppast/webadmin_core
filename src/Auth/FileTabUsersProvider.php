<?php

// namespace
namespace Ppast\Webadmin\Auth;


// clauses use



// classe
class FileTabUsersProvider extends UsersProvider
{
	protected $_file = NULL;
	protected $_cache = NULL;
    protected $_usersProperties = array('name', 'password', 'roles'); // par défaut, avant modification éventuelle dans listUsers
    
	
	
	// chemin vers fichiers
	public function __construct($file)
	{
		$this->_file = $file;
	}
	
	
    // obtenir les propriétés liées à l'utilisateur
	public function userProperties()
    {
        return $this->_usersProperties;
    }

    
    // énumérer les utilisateurs
	public function listUsers()
	{
		// utiliser cache de préférence
		if ( $this->_cache )
			return $this->_cache;
			
			
		$users = array();
		
			
		// si fichier existe
		if ( file_exists($this->_file) )
		{
			// ouvrir le fichier des utilisateurs
			$f = fopen($this->_file, 'r');
            
            // lire en-têtes
            $props = trim(fgets($f));
            if ( $props )
            {
                // ignorer cette première ligne
                $props = array_map('trim', explode(";", $props));
                
                // mémoriser propriétés définies
                $this->_usersProperties = $props;
            }
            
            
            // lire utilisateurs
			while ( $s = trim(fgets($f)) )
			{
                // tabulation colonnes avec ; 
                $utmp = array_map('trim', explode(";", $s));
                $user = array();
                $i = 0;

                // transformer la lecture indexée en tableau associatif propriété=valeur
                foreach ( $props as $k )
                    $user[$k] = $utmp[$i++];

                // par construction, le user est toujours la première info lue
                $users[$utmp[0]] = (object)$user;
			}

            fclose($f);
		}
		
		
		// maj cache
		$this->_cache = $users;		
		
		return $users;
	}
	
	
	// ajouter un utilisateur
	public function createUser(\stdClass $u)
	{
		$dochmod = false;
		$dochmodpath = false;


		// si fichier existe
		if ( file_exists($this->_file) )
		{			
			// si fichier protégé, le déprotéger temporairement
			if ( substr(decoct(fileperms($this->_file)),-4) == '0404' )
			{
				$dochmod = true;
				chmod($this->_file, 0604);
			}
			
			// ajouter en fin de fichier
			$f = fopen($this->_file, 'a');
		}
		
		else
		{
			// déprotéger éventuellement dossier parent, pour pouvoir créer un fichier
			$path = dirname($this->_file);
			if ( substr(decoct(fileperms($path)),-4) == '0505' )
			{
				$dochmodpath = true;
				$dochmod = true;
				chmod($path, 0705);
			}
			
            
            // création fichier
			$f = fopen($this->_file, 'w');

            // créer en-têtes
            fputs($f, implode(';', $this->_usersProperties) . "\r\n");
        }

        
        // écriture propriétés génériquement
        $line = array();
        foreach ( $this->_usersProperties as $prop )
            $line[] = $u->{$prop};
        
		fputs($f, implode(';', $line) . "\r\n");
		fclose($f);

		
		if ( $dochmod )
			chmod($this->_file, 0404);
		if ( $dochmodpath )
			chmod($path, 0505);
			
			
		// vider cache
		$this->_cache = NULL;
	}
	
	
	// effacer un utilisateur
	public function removeUser($uname)
	{
		// lister les utilisateurs
		$users = $this->listUsers();
		
		// supprimer user 
		unset($users[$uname]);

		$this->_commit($users);
	}
	

	// modifier un utilisateur
	public function updateUser($uname, \stdClass $u)
	{
		// lister les utilisateurs
		$users = $this->listUsers();
		
        // n'autoriser update que si user existe
        if ( array_key_exists($uname, $users) )
        {
            // mise à jour
            $users[$uname] = $u;
            $this->_commit($users);
        }
	}
	
	
	// écrire un tableau sur disque
	protected function _commit($users)
	{
		// si fichier protégé, le déprotéger temporairement
		$dochmod = false;
		if ( substr(decoct(fileperms($this->_file)),-4) == '0404' )
		{
			$dochmod = true;
			chmod($this->_file, 0604);
		}
		

        // écrire tableau users
		$f = fopen($this->_file, 'w');

        // créer en-têtes
        fputs($f, implode(';', $this->_usersProperties) . "\r\n");        
        
        foreach ( $users as $u )
        {
            // écriture propriétés génériquement
            $line = array();
            foreach ( $this->_usersProperties as $prop )
                $line[] = $u->{$prop};

            fputs($f, implode(';', $line) . "\r\n");
        }
		
        fclose($f);
		
        
		if ( $dochmod )
			chmod($this->_file, 0404);
			
			
		// vider cache
		$this->_cache = NULL;
	}
	
}

?>