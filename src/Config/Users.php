<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de gestion des utilisateurs : mots de passe et attribution niveau d'accès
class Users
{
	// utilisateurs
	protected static $_usersProvider = NULL;

	
	// niveau d'accès
	static $GUEST = 'guest';
	static $SUPERUSER = 'superuser';
	
	
	
	// fonction de hashage password
	public static function hashPasswd($passwd)
	{
		return hash('sha256', $passwd);
	}
	
	
	// obtenir stratégie de gestion des users
	public static function usersProvider()
	{
		return self::$_usersProvider;
	}
	
	
	// définir stratégie de gestion des users
	public static function setupUsersProvider()
	{
		// si provider non donné, créer user par défaut
		$provider = Core::$ROOT_USER_CFG->USERS_PROVIDER;
		if ( !$provider )
			die('Stratégie de gestion des utilisateurs non précisée. Arrêt imposé.');

		// créer la stratégie de gestion des users
		self::$_usersProvider = new $provider(Core::$ROOT_USER_CFG->USERS_PROVIDER_DATA);
		
		// si fichier n'existe pas ou fichier vide, créer un user admin provisoire
		if ( count(self::$_usersProvider->listUsers()) === 0 )
			self::$_usersProvider->createUser(
                    (object)array(
                            'name'      => 'admin',
                            'password'  => self::hashPasswd('password'),
                            'roles'     => self::$SUPERUSER
                        )
                );
	}
}


?>