<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe pour configuration racine
class Root extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($type, $params)
	{
		$obj = new Root($type, $params);

		switch ( $type )
		{
			case 'root' :
				Core::$ROOT_CFG = $obj;
				break;
			case 'user' :
				Core::$ROOT_USER_CFG = $obj;
				break;
		}
	}
}


?>