<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config 
class Base
{
	protected $params = NULL;
	public $name = NULL;
	
	
	// constructeur
	protected function __construct($name, $params)
	{
		$this->name = $name;
		$this->params = $params;
	}
	
	
	// accesseur magique
	public function __get($k)
	{
		if ( array_key_exists($k, $this->params) )
			return $this->params[$k];
		else
			return null;
	}
}


?>