<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe centrale pour config console
class Core
{
	// root www serveur
	static $ROOT = NULL;
		
	// chemin vers data user
	static $DATA_ROOT = NULL;
	
	// chemin vers webadmin (chemin du package ppast/webadmin ou ppast/Webadmin_console, selon le cas)
	static $WEBADMIN_ROOT = NULL;
	
	// chemin vers webadmin_core (chemin du package ppast/webadmin_core)
	static $WEBADMIN_CORE_ROOT = NULL;
	
	// chemin vers dossier appelant
	static $CALLER_ROOT = NULL;

	// config racine webadmin (instance Config\Root chargée depuis ppast/webadmin/RootConfig/root.php ou ppast/webadmin_console/RootConfig/root.php)
	static $ROOT_CFG = NULL;
	
	// config racine personnalisable (instance Config\Root chargée depuis dossier user/root_config/user.php )
	static $ROOT_USER_CFG = NULL;

	
	
	// config root
	static function rootConfig()
	{
		if ( file_exists(self::$WEBADMIN_ROOT . 'RootConfig/root.php') )
			include(self::$WEBADMIN_ROOT . 'RootConfig/root.php');
		if ( file_exists(self::$DATA_ROOT . 'root_config/user.php') )
			include(self::$DATA_ROOT . 'root_config/user.php');
	}

}



?>