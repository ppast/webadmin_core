<?php

// namespace
namespace Ppast\Webadmin\Includes;



class IncludesHelper 
{
	// nettoyer une chaine fournie par l'utilisateur en vue d'être intégrée dans mysql (ne pas oublier de faire mysql_real_escape_string ensuite)
	static function cm_sanitize($data)
	{
		// remove whitespaces (not a must though)
		//$data = filter_var(trim($data), FILTER_SANITIZE_STRING, !FILTER_FLAG_STRIP_LOW);
		$data = trim(strip_tags($data?$data:''));
		
		// apply stripslashes if magic_quotes_gpc is enabled // DEPRECATED PHP7.4
		/*if(get_magic_quotes_gpc())
			$data = stripslashes($data);*/ 
			
		// interdire la présence de commandes SQL
		$banlist = array
			(
			"/alter /i", "/insert /i", "/select /i", "/update /i", "/delete /i", "/distinct /i", "/having /i", "/truncate /i", "/replace /i",
			"/handler /i", "/like /i", "/procedure /i", "/limit /i", "/order by /i", "/group by /i" , "/drop /i", "/database /i"
			);
			
		$data = preg_replace($banlist, '', $data);
		
		return $data;
	}
	
	
	// nettoyer un tableau fourni par l'utilisateur en vue d'être intégrée dans mysql (ne pas oublier de faire mysql_real_escape_string ensuite)
	static function cm_sanitize_array(&$arr)
	{
		foreach ( $arr as $k => $v )
			$arr[$k] = self::cm_sanitize($v);
			
		return $arr;
	}
	
	
	// s'assurer que le dossier se termine par un slash
	static function ensureTrailingSlash($folder)
	{
		if ( preg_match('~/$~', $folder?$folder:'') )
			return $folder;
		else
			return $folder . '/';
	}


	// s'assurer que le dossier ne se termine pas par un slash
	static function ensureNoTrailingSlash($folder)
	{
		return rtrim($folder?$folder:'', '/');
	}


}



?>