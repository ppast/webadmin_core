<?php

// namespace
namespace Ppast\Webadmin\Includes;




// classe d'aide requete
final class RequestSecurityHelper
{
	/** 
	 * Calculer le jeton nonce à partir des paramètres fournis
	 *
	 * @param string $cnametoken Nom du cookie (utilisé pour reconstruire le nonce afin de le valider)
	 * @param string $c Valeur du cookie (utilisé pour reconstruire le nonce afin de le valider)
	 * @param string $secret Secret à utiliser dans la génération
	 * @return string Renvoie une chaine de 32+64 caractères ; les 64 caractères contiennent un hash de la valeur du nonce $c
	 */
	private static function _computeFormToken($cnametoken, $c, $secret)
	{
		return $cnametoken . hash_hmac('sha256', 'nonce:' . $c, $secret);
	}
	
	
	
	/** 
	 * Créer un nonce sous forme de cookie
	 *
	 * @return string Renvoie une chaine de 32+64 caractères + un cookie
	 */
	static function formToken()
	{
		// créer un cookie temporaire dont le nom est basé sur cette signature
		$c = bin2hex(random_bytes(32));						

		
		$cnametoken = md5('cookie-name!' . time());			// nom du cookie tel qu'apparaissant dans le jeton, et variant grâce au timestamp
		$cname = md5('md5-cookie-name!' . $cnametoken);		// nom réel du cookie crypté, pour ne pas qu'il apparaisse tel quel dans le jeton
		setcookie($cname, $c);								// définir le cookie (expire à la fermeture du navigateur)
		$_COOKIE[$cname] = $c;								// rendre disponible dès maintenant
		
		return self::_computeFormToken($cnametoken, $c, basename(__FILE__));
	}
	
	
	
	/**
	 * Vérifier un jeton de formulaire
	 *
	 * @param string $t Jeton à vérifier
	 * @return bool
	 */
	static function checkFormToken($t)
	{
		// reconstituer le token à partir des infos
		if ( is_null($t) )
			$t = '';	// si $t est NULL, hash_equals émettra un E_WARNING et ce n'est pas souhaitable

		
		$cnametoken = substr($t, 0, 32);
		$cname = md5('md5-cookie-name!' . $cnametoken);

		// valider avec cookies
		$b = hash_equals(self::_computeFormToken($cnametoken, $_COOKIE[$cname], basename(__FILE__)), $t);
		
		// supprimer le cookie
		setcookie($cname, '', time()-3600);
		unset($_COOKIE[$cname]);
		return $b;
	}
	
	
	
	/**
	 * Calculer le jeton à expiration
	 *
    * @param int $graceperiod Number of seconds, minutes or hours of the validity delay
    * @param string $period Provide either 's', 'm' or 'h' to set the unit for the $graceperiod parameter
    * @param string $secret A secret to use to generate the token
    * @param string $unid Unique ID
    * @param string $ts Timesamp
    * @return string A timestamp token with a embedded expiration time
	 */
	private static function _computeTimestampToken($graceperiod = 60, $period = "s", $secret = 'token', $unid = NULL, $ts = NULL)
	{
 		return hash_hmac('sha256', "$graceperiod;$period;$unid;$ts", $secret) . ";$graceperiod;$period;$unid;" . dechex($ts);
	}

	
	
	/**
    * Create a token with a expiration time
    * 
    * By default, the token expires 60 seconds later to create the token, you may provide the validity delay,
    * the unit of the delay (seconds, minutes or hours) and a secret. If no parameters, default values will be used
    * 
    * @param int $graceperiod Number of seconds, minutes or hours of the validity delay
    * @param string $period Provide either 's', 'm' or 'h' to set the unit for the $graceperiod parameter
    * @param string $secret A secret to use to generate the token
    * @return string A timestamp token with a embedded expiration time
    */
	static function createTimestampToken($graceperiod = 60, $period = "s", $secret = 'token')
	{
		if ( $graceperiod > 255 )
            // we can only encode values less than 256, as we are dealing with bytes.
            // if we have an overflow and the delay unit is in seconds, we convert the delay to minutes
			if ( $period == 's' )
				return self::createTimestampToken(round($graceperiod / 60), 'm', $secret);
				
            // if we have an overflow and the delay unit is in minutes, we convert the delay to hours
			else if ( $period == 'm' )
				return self::createTimestampToken(round($graceperiod / 60), 'h', $secret);
				
            // if we have an overflow and the delay unit is in hours, we convert the delay to 255 hours
			else
				return self::createTimestampToken(255, 'h', $secret);
			
		$ts = time();
		$unid = bin2hex(random_bytes(32)); 
			
		// if delay is < 16, we must use a leading 0
		$graceperiod = dechex($graceperiod);
		if ( strlen($graceperiod) == 1 )
			$graceperiod = "0$graceperiod";

		
		return self::_computeTimestampToken($graceperiod, $period, $secret, $unid, $ts);
	}
	
	
	/**
     * Check a timestamp token
     * 
     * @param string $token The token to check (valid, not altered, not expired)
     * @param string $secret The secret used to generate the token
     * @return bool If altered OR expired, returning FALSE
     */
	static function checkTimestampToken($token, $secret = 'token')
	{
		// checking format
		if ( !preg_match('/^[a-fA-F0-9]{64};[a-fA-F0-9]{2};(h|m|s);[a-fA-F0-9]+;[a-fA-F0-9]+$/', $token) )
			return false;
	

		list($dummy, $graceperiod, $period, $unid, $ts) = explode(';', $token);
		$dummy = $dummy;
		$graceperiod = hexdec($graceperiod);
		$ts = hexdec($ts);
		
		// calc the delay in seconds
		if ( $period == 'h' )
			$graceperiod_seconds = $graceperiod*60*60;
		else if ( $period == 'm' )
			$graceperiod_seconds = $graceperiod*60;
		else
			$graceperiod_seconds = $graceperiod;
			
		
		// create the token with extracted data and check it has not been altered ; then check the time validity 
		if ( is_null($token) )
			$token = '';	// si TOKEN = NULL, hash_equals émettra un E_WARNING ce qui n'est pas souhaitable
		
		return (hash_equals(self::_computeTimestampToken($graceperiod_seconds, $period, $secret, $unid, $ts), $token))
				&&
				($ts + $graceperiod_seconds > time());
	}
	
}


?>