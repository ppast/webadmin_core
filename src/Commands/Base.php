<?php

// namespace
namespace Ppast\Webadmin\Commands;



class Base 
{
	// contenu résultant
	protected $_body = NULL;
	protected $_status = NULL;
	protected $_message = NULL;
	protected $_statusMetadata = NULL;
	


	// par défaut, ne rien faire
	protected function _renderFooter()
	{
	}
	
	
	// par défaut, ne rien faire
	protected function _renderCSS_JS()
	{
	}
	
	
	// par défaut, ne rien faire
	protected function _renderBody()
	{
		if ( is_string($this->_body) )
			echo $this->_body;
	}
	
	
	// body existe ?
	public function hasBody()
	{
		return $this->_body ? true:false;
	}
	
	
	public function getBody()
	{
		return $this->_body;
	}
	
	
	public function getStatus()
	{
		return $this->_status;
	}
	
	
	public function getMessage()
	{
		return $this->_message;
	}
	
	
	public function getStatusMetadata($k = NULL)
	{
		if ( is_null($k) )
			return $this->_statusMetadata;
		else
			return array_key_exists($k, $this->_statusMetadata) ? $this->_statusMetadata[$k] : null;
	}
	
	
	// affichage
	public function render()
	{
		// styles et js
		$this->_renderCSS_JS();
		
		// contenu
		$this->_renderBody();	
	
		// pied de page
		$this->_renderFooter();
	}
	
	
	// renvoyer un statut et un body
	public function status($b, $message = NULL, $feedback = NULL, $autofeedback = false)
	{
		$this->_body = $feedback;
		$this->_status = $b;
		$this->_message = $message;
		$this->_statusMetadata = array('autofeedback'=>$autofeedback);
		return $b;
//		return array('status'=>$b, 'message'=>$message, 'autofeedback' => $autofeedback);
	}
	

}

?>